var assert = require('assert');
var sudoku = require('../js/sudoku.js');

var toPuzzle = function(str) {
  return str.split("").map(function(s) { return parseInt(s); });
}

var puzzle = toPuzzle(
    "300040005" +
    "020008493" +
    "000091000" +
    "003000500" +
    "007805200" +
    "001000900" +
    "000210000" +
    "512900060" +
    "800070004");

var solution = toPuzzle(
    "398642715" +
    "126758493" +
    "745391682" +
    "483129576" +
    "967835241" +
    "251467938" +
    "674213859" +
    "512984367" +
    "839576124");


describe('Sudoku', function() {

  describe('#available()', function() {
    it('should return the correct available numbers', function() {
      assert.deepEqual([1, 2, 3, 5], sudoku.available(79, puzzle));
    })
  })

  describe('#solve()', function() {
    it('should solve a sudoku problem', function() {
      assert.deepEqual(solution, sudoku.solve(puzzle));
    })
  })

})
