var _ = require('lodash-contrib');

/**
* Solver for sudoku puzzles
*
* @method solve
* @param {Array[Int]} puzzle
* @return {Array[Int] | Null} the solution, or null, if no solution exists
*/
var solve = function(puzzle) {
  "use strict";

  var i = puzzle.indexOf(0);
  if (i === -1) { return puzzle; } // solved
  else {
    var remain = available(i, puzzle);
    if (!remain.length) { return null; } // dead-end
    else {
      for (var c=0; c < remain.length; c++) {
        var result = solve(puzzle.slice(0, i).
          concat([remain[c]], puzzle.slice(i+1, 81))); // recurse
        if (result) { return result; }
      }
      return null; // dead-end
    }
  }
};

/**
* Finds available numbers at a square in the puzzle
*
* @method available
* @param {Int} index of the square
* @puzzle {Array[Int]} the puzzle
*/
var available = function(i, puzzle) {
  "use strict";

  var s = Math.floor(i / 27) * 9 + Math.floor(i % 9 / 3);
  var p9 = _.chunk(puzzle, 9);
  var p3 = _.chunk(puzzle, 3);

  var rowNums = p9[Math.floor(i / 9)];
  var colNums = _.range(9).map(function(n) { return p9[n][i % 9]; });
  var squNums = p3[s].concat(p3[s + 3], p3[s + 6]);
  var allNums = _.uniq(rowNums.concat(colNums, squNums));

  return _.range(1, 10).filter(function(n) { return !_.contains(allNums, n); });
};

module.exports.solve = solve;
module.exports.available = available;
